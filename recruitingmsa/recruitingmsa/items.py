# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import Join, MapCompose, TakeFirst
from w3lib.html import remove_tags, remove_tags_with_content


def clean_text(x):
    if x is not None:
        elems = x.replace('\n', '').replace('  ', '').replace(', Inc', ' Inc').replace(' and ', ',').split(',')
        return [elem.strip() for elem in elems if elem.strip()]

def replace_br_tags(x):
    if x is not None:
        return x.replace('<br>', ', ').replace('<br/>', ', ')

def replace_li_tags(x):
    if x is not None:
        return x.replace('<li>', '').replace('</li>', ', ')

def remove_content_between(x, char1, char2):
    start = x.find(char1)
    end = x.find(char2)
    if start >= 0 < end:
        return remove_content_between(x.replace(x[start:end+1], ' '), char1, char2)
    return x

def replace_brackets(x):
    return remove_content_between(x, '[', ']')

def replace_parentheses(x):
    return remove_content_between(x, '(', ')')

def remove_span_tags_with_content(x):
    return remove_tags_with_content(x, which_ones=('span',))

def remove_content(x):
    return remove_tags_with_content(x, which_ones=('small','b'))


class GameItem(scrapy.Item):

    name = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst(),
    )
    src_img = scrapy.Field(
        output_processor=TakeFirst(),
    )
    publishers = scrapy.Field(
        input_processor=MapCompose(remove_span_tags_with_content, replace_li_tags, replace_br_tags, remove_tags, replace_brackets, replace_parentheses, clean_text),
    )
    developers = scrapy.Field(
        input_processor=MapCompose(replace_br_tags, remove_tags, replace_brackets, replace_parentheses, clean_text),
    )
    artists = scrapy.Field(
        input_processor=MapCompose(replace_br_tags, remove_tags, replace_parentheses, clean_text),
    )
    composers = scrapy.Field(
        input_processor=MapCompose(replace_br_tags, remove_tags, replace_brackets, replace_parentheses, clean_text),
    )
    platform = scrapy.Field(
        input_processor=MapCompose(remove_content, replace_br_tags, replace_li_tags, remove_tags, replace_brackets, replace_parentheses, clean_text),
    )
    release = scrapy.Field()
    genre = scrapy.Field(
        input_processor=MapCompose(replace_br_tags, remove_tags, replace_brackets, replace_parentheses, clean_text),
    )
    mode = scrapy.Field(
        input_processor=MapCompose(replace_br_tags, remove_tags, replace_parentheses, clean_text),
    )
