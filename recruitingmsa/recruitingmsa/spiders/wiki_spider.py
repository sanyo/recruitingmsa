import scrapy, datetime, re
from scrapy.loader import ItemLoader
from recruitingmsa.items import GameItem
from w3lib.html import remove_tags
from recruitingmsa.items import replace_parentheses


def format_date(s_date):
    date_patterns = [
        ('\d{4} [A-z]+ \d{1,2}', '%Y %B %d', '%d/%m/%Y'),
        ('[A-z]+ \d{1,2}, \d{4}', '%B %d, %Y', '%d/%m/%Y'),
        ('\d{1,2} [A-z]+ \d{4}', '%d %B %Y', '%d/%m/%Y'),
        ('[A-z]+ \d{4}', '%B %Y', '%m/%Y'),
        ('\d{4}', '%Y', '%Y')
    ]

    for re_pattern, dt_pattern, fmt in date_patterns:
        match = re.search(re_pattern, s_date)
        if match is not None:
            try:
                return datetime.datetime.strptime(match.group(), dt_pattern).strftime(fmt)
            except:
                pass
    return s_date


class WikiSpider(scrapy.Spider):
    name = "wikigames"
    start_urls = [
        'https://en.wikipedia.org/wiki/Category:Articles_using_Infobox_video_game_using_locally_defined_parameters',
    ]
    base_wiki_url = 'https://en.wikipedia.org{}'


    def __init__(self, limit=5, *args, **kwargs):
        super(WikiSpider, self).__init__(*args, **kwargs)
        self.limit = int(limit)


    def parse_game_info(self, response):
        l = ItemLoader(item=GameItem(), response=response)
        l.add_xpath('name', '//h1[@class="firstHeading"]')

        info = response.xpath('//table[@class="infobox hproduct"]/tbody')
        src_img = info.xpath('.//td/a[@class="image"]/img/@src').get()
        if src_img:
            l.add_value('src_img', response.urljoin(src_img))
        l.add_value('publishers', info.xpath('.//th[contains(., "Publisher(s)")]/../td').get())
        l.add_value('developers', info.xpath('.//th[contains(., "Developer(s)")]/../td').get())
        l.add_value('artists', info.xpath('.//th[contains(., "Artist(s)")]/../td').get())
        l.add_value('composers', info.xpath('.//th[contains(., "Composer(s)")]/../td').get())
        l.add_value('platform', info.xpath('.//th[contains(., "Platform(s)")]/../td').get())

        info_release = []
        all_release_platform = []
        release_selector = info.xpath('.//th[contains(., "Release")]/../td')

        release_date = release_selector.xpath('normalize-space(./div/div | ./../*[count(a)=1]/a)').extract_first()
        if not release_date:
            release_date = release_selector.xpath('normalize-space(./text())').extract_first() or ''

        is_first_time = True
        for li in release_selector.xpath('./div/ul/li/div[@class="plainlist"] | ./div[@class="plainlist"]'):
            release_platform = list(set(li.xpath('./preceding-sibling::*[self::b|self::i|self::p/b] | ./b').extract()) - set(all_release_platform))
            all_release_platform += release_platform
            regions = []
            platform = (replace_parentheses((remove_tags(', '.join(release_platform))).replace('\n', ''))).strip()

            if platform == '' and not is_first_time:  # Avoid duplicate regions
                continue
            is_first_time = False

            for r in li.xpath('./ul/li | ./../div[count(preceding-sibling::b)={}]/ul/li'.format(len(all_release_platform))):
                regions.append({
                    'region': (r.xpath('string(.//span)').extract_first()).replace(':', ''),
                    'date': format_date((r.xpath('./text() | ./ul/li/text()').extract_first()).strip())
                })

            info_release.append({
                'platform': platform,
                'regions': regions
            })

        l.add_value('release', [{'main_date': format_date(release_date), 'info': info_release}])
        l.add_value('genre', info.xpath('.//th[contains(., "Genre(s)")]/../td').get())
        l.add_value('mode', info.xpath('.//th[contains(., "Mode(s)")]/../td').get())

        yield l.load_item()


    def parse(self, response):
        for game in response.css('div.mw-category-group ul li'):
            url = game.xpath('a/@href').extract_first()
            if url is not None:
                yield scrapy.Request(self.base_wiki_url.format(url), callback=self.parse_game_info)
                self.limit -= 1
                if self.limit == 0:
                    break

        if self.limit > 0:
            next_page = response.xpath('//div[@id="mw-pages"]/a[contains(., "next page")]/@href').extract_first()
            if next_page is not None:
                next_page = response.urljoin(next_page)
                yield scrapy.Request(next_page, callback=self.parse)
