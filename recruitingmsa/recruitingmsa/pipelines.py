# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class RecruitingmsaPipeline(object):
    def process_item(self, item, spider):
        item.setdefault('src_img', '')
        item.setdefault('publishers', [])
        item.setdefault('developers', [])
        item.setdefault('artists', [])
        item.setdefault('composers', [])
        item.setdefault('platform', [])
        item.setdefault('release', [])
        item.setdefault('genre', [])
        item.setdefault('mode', [])
        return item
