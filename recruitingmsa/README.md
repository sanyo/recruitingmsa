How to run
================================

Run the following command line to get a JSON file as a result:

`scrapy crawl wikigames -o filename.json`


Use the "limit" argument to indicate the number of scraped items you want (default value is 5).
For example:

`scrapy crawl wikigames -a limit=1000 -o filename.json`
